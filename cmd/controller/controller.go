package main

import (
	clientset "gitlab.com/ngilmant/training-go-3/pkg/generated/clientset/versioned"
	informers "gitlab.com/ngilmant/training-go-3/pkg/generated/informers/externalversions"
	"gitlab.com/ngilmant/training-go-3/pkg/controller"
	"gitlab.com/ngilmant/training-go-3/pkg/signals"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/klog"
	"os"
	"time"
)

func main() {
	klog.InitFlags(nil)

	config, err :=  kubeConfig()
	if err != nil {
		klog.Errorf("Error occurs during configuraing kube client %v\n", err)
		panic(err)
	}

	stopfactoryCh := signals.SetupSignalHandler()

	//build Kubernetes client.
	kclient := kubernetes.NewForConfigOrDie(config)
	info, err := kclient.Discovery().ServerVersion()

	if err != nil {
		klog.Errorf("unable to connect kubernetes api %v\n", err)
		panic(err)
	}
	klog.Info("[kubernetes-client] connected to kubernetes version %s.%s\n", info.Major, info.Minor)

	nicoclient := clientset.NewForConfigOrDie(config)
	info, err = nicoclient.Discovery().ServerVersion()

	if err != nil {
		klog.Errorf("unable to connect clientset api %v\n", err)
		panic(err)
	}
	klog.Info("[clientset-client] connected to kubernetes version %s.%s\n", info.Major, info.Minor)


	kubeInformerFactory := kubeinformers.NewSharedInformerFactory(kclient, time.Second*30)
	nicoInformerFactory := informers.NewSharedInformerFactory(nicoclient, time.Second*30)

	controller := controller.NewController(kclient, nicoclient,
		kubeInformerFactory.Apps().V1().Deployments(),
		kubeInformerFactory.Core().V1().Services(),
		nicoInformerFactory.Nicocontroller().V1alpha1().SuperNicos())

	// notice that there is no need to run Start methods in a separate goroutine. (i.e. go kubeInformerFactory.Start(stopCh)
	// Start method is non-blocking and runs all registered informers in a dedicated goroutine.
	kubeInformerFactory.Start(stopfactoryCh)
	nicoInformerFactory.Start(stopfactoryCh)

	if err = controller.Run(2, stopfactoryCh); err != nil {
		klog.Fatalf("Error running controller: %s", err.Error())
	}
}


func kubeConfig() (*rest.Config, error) {
	if kubeConfig := os.Getenv("KUBECONFIG"); len(kubeConfig) > 0 {
		return clientcmd.BuildConfigFromFlags("", kubeConfig)
	}
	return rest.InClusterConfig()
}
