# training-go-3

GO Training Tour Exercise 3 (advanced)

# Level

Advanced

# Topics

* k8s Custom Resources
* Client API Code Generator
* Custom Resource Controller
* Informer - Custom Resource Handler
* Work Queue
* Item Processing

# Content

Transform The Deployer of the Exercise 2 to a Custom Resource (https://gitlab.com/ngilmant/training-go-2).

The Custom Resource will be able to :

- Deploy new components
- Listen state changes and behaves accordingly the Desired State vs Actual State.

