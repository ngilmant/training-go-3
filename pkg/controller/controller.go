package controller

import (
	"fmt"
	"gitlab.com/ngilmant/training-go-3/pkg/apis/nicocontroller/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	clientset "gitlab.com/ngilmant/training-go-3/pkg/generated/clientset/versioned"
	listers "gitlab.com/ngilmant/training-go-3/pkg/generated/listers/nicocontroller/v1alpha1"
	informers "gitlab.com/ngilmant/training-go-3/pkg/generated/informers/externalversions/nicocontroller/v1alpha1"
	nicoscheme "gitlab.com/ngilmant/training-go-3/pkg/generated/clientset/versioned/scheme"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	applisters "k8s.io/client-go/listers/apps/v1"
	appsinformers "k8s.io/client-go/informers/apps/v1"
	corelisters "k8s.io/client-go/listers/core/v1"
	coreinformers "k8s.io/client-go/informers/core/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
	"k8s.io/klog"
	"time"
)

const (
	controllerAgentName = "supernico-controller"
	// MessageResourceExists is the message used for Events when a resource
	// fails to sync due to a Deployment already existing
	MessageResourceExists = "Resource %q already exists and is not managed by Foo"
	// ErrResourceExists is used as part of the Event 'reason' when a Foo fails
	// to sync due to a Deployment of the same name already existing.
	ErrResourceExists = "ErrResourceExists"
)

// Controller is the controller implementation for Foo resources
type Controller struct {

	// kubeclientset is a standard kubernetes clientset
	kubeclientset kubernetes.Interface
	// nicoclientset is a clientset for our own API group
	nicoclientset clientset.Interface

	deploymentsLister applisters.DeploymentLister
	deploymentsSynced cache.InformerSynced

	servicesLister corelisters.ServiceLister
	servicesSynced	  cache.InformerSynced

	nicoLister        listers.SuperNicoLister
	nicoSynced        cache.InformerSynced

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface

	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

// NewController returns a new SuperNico controller
func NewController(
	kubeclientset kubernetes.Interface,
	nicoclientset clientset.Interface,
	deploymentInformer appsinformers.DeploymentInformer,
	serviceInformer coreinformers.ServiceInformer,
	nicoInformer informers.SuperNicoInformer) *Controller {

	// Add sample-controller types to the default Kubernetes Scheme so Events can be
	// logged for sample-controller types.

	utilruntime.Must(nicoscheme.AddToScheme(scheme.Scheme))
	klog.V(4).Info("Creating event broadcaster")
	eventBroadcaster := record.NewBroadcaster()
	eventBroadcaster.StartLogging(klog.Infof)
	eventBroadcaster.StartRecordingToSink(&typedcorev1.EventSinkImpl{Interface: kubeclientset.CoreV1().Events("")})
	recorder := eventBroadcaster.NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	controller := &Controller{
		kubeclientset:		kubeclientset,
		nicoclientset:  	nicoclientset,
		deploymentsLister: 	deploymentInformer.Lister(),
		deploymentsSynced:	deploymentInformer.Informer().HasSynced,
		servicesLister:     serviceInformer.Lister(),
		servicesSynced:     serviceInformer.Informer().HasSynced,
		nicoLister:			nicoInformer.Lister(),
		nicoSynced:			nicoInformer.Informer().HasSynced,
		workqueue:			workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "SuperNico"),
		recorder:			recorder,
	}// Create event broadcaster

	klog.Info("Setting up event handlers")

	// SuperNico Informer
	// Set up an event handler for when SuperNico resources change
	nicoInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: controller.enqueueNico,
		UpdateFunc: func(old, new interface{}) {
			controller.enqueueNico(new)
		},
		DeleteFunc: controller.enqueueNico,
	})

	// Deployment Informer.
	deploymentInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: controller.handleObject,
		UpdateFunc: func(old, new interface{}) {
			newDepl := new.(*appsv1.Deployment)
			oldDepl := old.(*appsv1.Deployment)
			if newDepl.ResourceVersion == oldDepl.ResourceVersion {
				// Periodic resync will send update events for all known Deployments.
				// Two different versions of the same Deployment will always have different RVs.
				return
			}
			controller.handleObject(new)
		},
		DeleteFunc: controller.handleObject,
	})

	// Service Informer.
	serviceInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: controller.handleObject,
		UpdateFunc: func(old, new interface{}) {
			newSvc := new.(*corev1.Service)
			oldSvc :=old.(*corev1.Service)
			if newSvc.ResourceVersion == oldSvc.ResourceVersion {
				return
			}
			controller.handleObject(new)
		},
	})

	return controller
}

// Run will set up the event handlers for types we are interested in, as well
// as syncing informer caches and starting workers. It will block until stopCh
// is closed, at which point it will shutdown the workqueue and wait for
// workers to finish processing their current work items.
func (c *Controller) Run(threadiness int, stopCh <-chan struct{}) error {
	// don't let panics crash the process
	defer utilruntime.HandleCrash()
	// make sure the work queue is shutdown which will trigger workers to end
	defer c.workqueue.ShutDown()

	// Start the informer factories to begin populating the informer caches
	klog.Info("Starting SuperNico controller")

	// Wait for the caches to be synced before starting workers
	klog.Info("Waiting for informer caches to sync")
	if ok := cache.WaitForCacheSync(stopCh, c.deploymentsSynced, c.nicoSynced); !ok {
		return fmt.Errorf("failed to wait for caches to sync")
	}

	klog.Info("Starting workers")
	// Launch two workers to process SuperNico resources
	for i := 0; i < threadiness; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	klog.Info("Started workers")
	<-stopCh
	klog.Info("Shutting down workers")

	return nil
}

// runWorker is a long-running function that will continually call the
// processNextWorkItem function in order to read and process a message on the
// workqueue.
func (c *Controller) runWorker() {
	for c.processNextWorkItem() {
	}
}

// processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {

	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		defer c.workqueue.Done(obj)
		var key string
		var ok bool

		//We expect string coming from the work queue.
		if key, ok = obj.(string); !ok {
			c.workqueue.Forget(obj)
			utilruntime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}

		// Run the syncHandler, passing it the namespace/name string of the
		// SuperNico resource to be synced.
		if err := c.syncHandler(key); err != nil {
			// Put the item back on the workqueue to handle any transient errors.
			c.workqueue.AddRateLimited(key)
			return fmt.Errorf("error syncing '%s': %s, requeuing", key, err)
		}
		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		klog.Infof("Successfully synced '%s'", key)

		return nil

	}(obj)

	if err != nil {
		utilruntime.HandleError(err)
		return true
	}

	return true
}



// enqueueNico takes a SuperNico resource and converts it into a namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than SuperNico.
func (c *Controller) enqueueNico(obj interface{}) {
	var key string
	var err error
	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		utilruntime.HandleError(err)
		return
	}
	c.workqueue.Add(key);
}

// handleObject will take any resource implementing metav1.Object and attempt
// to find the SuperNico resource that 'owns' it. It does this by looking at the
// objects metadata.ownerReferences field for an appropriate OwnerReference.
// It then enqueues that SuperNico resource to be processed. If the object does not
// have an appropriate OwnerReference, it will simply be skipped.
func (c *Controller) handleObject(obj interface{}) {
	var object metav1.Object
	var ok bool
	if object, ok = obj.(metav1.Object); !ok {
		tombstone, ok := obj.(cache.DeletedFinalStateUnknown)
		if !ok {
			utilruntime.HandleError(fmt.Errorf("error decoding object, invalid type"))
			return
		}
		object, ok = tombstone.Obj.(metav1.Object)
		if !ok {
			utilruntime.HandleError(fmt.Errorf("error decoding object tombstone, invalid type"))
			return
		}
		klog.V(4).Infof("Recovered deleted object '%s' from tombstone", object.GetName())
	}

	klog.V(4).Infof("Processing object: %s", object.GetName())

	if ownerRef := metav1.GetControllerOf(object); ownerRef != nil {

		// If this object is not owned by a SuperNico, we should not do anything more
		// with it.
		if ownerRef.Kind != "SuperNico" {
			return
		}
		supernico, err := c.nicoLister.SuperNicos(object.GetNamespace()).Get(ownerRef.Name)
		if err != nil {
			klog.V(4).Infof("ignoring orphaned object '%s' of superNico '%s'", object.GetSelfLink(), ownerRef.Name)
			return
		}
		c.enqueueNico(supernico)
		return
	}

}


// syncHandler compares the actual state with the desired, and attempts to
// converge the two. It then updates the Status block of the SuperNico resource
// with the current status of the resource.
func (c *Controller) syncHandler(key string) error {
	// Convert the namespace/name string into a distinct namespace and name
	namespace, name, err := cache.SplitMetaNamespaceKey(key)

	if err != nil {
		utilruntime.HandleError(fmt.Errorf("invalid resource key: %s", key))
		return nil
	}

	// Get the SuperNico resource with this namespace/name
	supernico, err :=c.nicoLister.SuperNicos(namespace).Get(name)

	if err != nil {
		// The SuperNico resource may no longer exist, in which case we stop
		// processing.
		if errors.IsNotFound(err) {
			utilruntime.HandleError(fmt.Errorf("superNico '%s' in work queue no longer exists - delete the children", key))
			//Delete
			return nil
		}
		return err
	}

	deploymentName := supernico.Spec.DeploymentName
	if deploymentName == "" {
		// We choose to absorb the error here as the worker would requeue the
		// resource otherwise. Instead, the next time the resource is updated
		// the resource will be queued again.
		utilruntime.HandleError(fmt.Errorf("%s: deployment name must be specified", key))
		return nil
	}

	// Get the deployment with the name specified
	// ******************************************
	deployment, err := c.deploymentsLister.Deployments(supernico.Namespace).Get(deploymentName)

	// If the resource doesn't exist, we'll create it
	if errors.IsNotFound(err) {
		deployment, err = c.kubeclientset.AppsV1().Deployments(supernico.Namespace).Create(newDeployment(supernico))
	}

	if err != nil {
		return err
	}

	// If the Deployment is not controlled by this SuperNico resource, we should log
	// a warning to the event recorder and return
	if !metav1.IsControlledBy(deployment, supernico) {
		msg := fmt.Sprintf(MessageResourceExists, deployment.Name)
		c.recorder.Event(supernico, corev1.EventTypeWarning, ErrResourceExists, msg)
		return fmt.Errorf(msg)
	}

	// If this number of the replicas on the SuperNicop resource is specified, and the
	// number does not equal the current desired replicas on the Deployment, we
	// should update the Deployment resource.
	if supernico.Spec.Replicas != nil && supernico.Spec.Replicas != deployment.Spec.Replicas {
		klog.V(4).Infof("SuperNico %s replicas: %d, deployment replicas: %d", name, *supernico.Spec.Replicas, *deployment.Spec.Replicas)
		deployment, err = c.kubeclientset.AppsV1().Deployments(supernico.Namespace).Update(newDeployment(supernico))
	}

	// If an error occurs during Update, we'll requeue the item so we can
	// attempt processing again later. THis could have been caused by a
	// temporary network failure, or any other transient reason.
	if err != nil {
		return err
	}


	// Get The Service with the name specified
	// ***************************************
	serviceName := supernico.Spec.DeploymentName
	service, err := c.servicesLister.Services(supernico.Namespace).Get(serviceName)

	if errors.IsNotFound(err) {
		service, err = c.kubeclientset.CoreV1().Services(supernico.Namespace).Create(newService(supernico))
	}

	if err != nil {
		return err
	}

	if !metav1.IsControlledBy(service, supernico) {
		msg := fmt.Sprintf(MessageResourceExists, service.Name)
		c.recorder.Event(supernico, corev1.EventTypeWarning, ErrResourceExists, msg)
		return fmt.Errorf(msg)
	}


	// Finally, we update the status block of the Foo resource to reflect the
	// current state of the world
	err = c.updateSuperNicoDeploymentStatus(supernico, deployment)
	if err != nil {
		return err
	}

	return nil
}

func newDeployment(supernico *v1alpha1.SuperNico) *appsv1.Deployment {

	deployment := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name: 	supernico.Spec.DeploymentName,
			Namespace: supernico.Namespace,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(supernico, v1alpha1.SchemeGroupVersion.WithKind("SuperNico")),
			},
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: supernico.Spec.Replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string {
					"app": supernico.Spec.DeploymentName,
					"controller": supernico.Name,
				},
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"app": supernico.Spec.DeploymentName,
						"controller": supernico.Name,
					},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name: supernico.Spec.Container.Name,
							Image: supernico.Spec.Container.Image,
							Ports: []corev1.ContainerPort{
								{
									Name: "http",
									Protocol: corev1.ProtocolTCP,
									ContainerPort: supernico.Spec.Container.ContainerPort,
								},
							},
						},
					},
				},
			},
		},
	}

	return deployment
}

func newService(supernico *v1alpha1.SuperNico) *corev1.Service {

	service := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name: 	supernico.Spec.DeploymentName,
			Namespace: supernico.Namespace,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(supernico, v1alpha1.SchemeGroupVersion.WithKind("SuperNico")),
			},
		},
		Spec: corev1.ServiceSpec {
			Type:
				corev1.ServiceType(supernico.Spec.Service.Type),
			Selector: map[string]string {
				"app": supernico.Spec.DeploymentName,
			},
			Ports: []corev1.ServicePort{
				{
					Port: supernico.Spec.Container.ContainerPort,
					Protocol: corev1.ProtocolTCP,
					TargetPort: intstr.FromInt(int(supernico.Spec.Container.ContainerPort)),
				},
			},
		},
	}

	return service
}




func (c *Controller) updateSuperNicoDeploymentStatus(supernico *v1alpha1.SuperNico, deployment *appsv1.Deployment) error {

	nicoCopy := supernico.DeepCopy()
	_, err :=c.nicoclientset.NicocontrollerV1alpha1().SuperNicos(supernico.Namespace).Update(nicoCopy)
	return err
}