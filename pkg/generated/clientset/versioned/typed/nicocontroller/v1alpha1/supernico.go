/*
Copyright Super Nico.
*/
// Code generated by client-gen. DO NOT EDIT.

package v1alpha1

import (
	"time"

	v1alpha1 "gitlab.com/ngilmant/training-go-3/pkg/apis/nicocontroller/v1alpha1"
	scheme "gitlab.com/ngilmant/training-go-3/pkg/generated/clientset/versioned/scheme"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
)

// SuperNicosGetter has a method to return a SuperNicoInterface.
// A group's client should implement this interface.
type SuperNicosGetter interface {
	SuperNicos(namespace string) SuperNicoInterface
}

// SuperNicoInterface has methods to work with SuperNico resources.
type SuperNicoInterface interface {
	Create(*v1alpha1.SuperNico) (*v1alpha1.SuperNico, error)
	Update(*v1alpha1.SuperNico) (*v1alpha1.SuperNico, error)
	Delete(name string, options *v1.DeleteOptions) error
	DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error
	Get(name string, options v1.GetOptions) (*v1alpha1.SuperNico, error)
	List(opts v1.ListOptions) (*v1alpha1.SuperNicoList, error)
	Watch(opts v1.ListOptions) (watch.Interface, error)
	Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.SuperNico, err error)
	SuperNicoExpansion
}

// superNicos implements SuperNicoInterface
type superNicos struct {
	client rest.Interface
	ns     string
}

// newSuperNicos returns a SuperNicos
func newSuperNicos(c *NicocontrollerV1alpha1Client, namespace string) *superNicos {
	return &superNicos{
		client: c.RESTClient(),
		ns:     namespace,
	}
}

// Get takes name of the superNico, and returns the corresponding superNico object, and an error if there is any.
func (c *superNicos) Get(name string, options v1.GetOptions) (result *v1alpha1.SuperNico, err error) {
	result = &v1alpha1.SuperNico{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("supernicos").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do().
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of SuperNicos that match those selectors.
func (c *superNicos) List(opts v1.ListOptions) (result *v1alpha1.SuperNicoList, err error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	result = &v1alpha1.SuperNicoList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("supernicos").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Do().
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested superNicos.
func (c *superNicos) Watch(opts v1.ListOptions) (watch.Interface, error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	opts.Watch = true
	return c.client.Get().
		Namespace(c.ns).
		Resource("supernicos").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Watch()
}

// Create takes the representation of a superNico and creates it.  Returns the server's representation of the superNico, and an error, if there is any.
func (c *superNicos) Create(superNico *v1alpha1.SuperNico) (result *v1alpha1.SuperNico, err error) {
	result = &v1alpha1.SuperNico{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("supernicos").
		Body(superNico).
		Do().
		Into(result)
	return
}

// Update takes the representation of a superNico and updates it. Returns the server's representation of the superNico, and an error, if there is any.
func (c *superNicos) Update(superNico *v1alpha1.SuperNico) (result *v1alpha1.SuperNico, err error) {
	result = &v1alpha1.SuperNico{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("supernicos").
		Name(superNico.Name).
		Body(superNico).
		Do().
		Into(result)
	return
}

// Delete takes name of the superNico and deletes it. Returns an error if one occurs.
func (c *superNicos) Delete(name string, options *v1.DeleteOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("supernicos").
		Name(name).
		Body(options).
		Do().
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *superNicos) DeleteCollection(options *v1.DeleteOptions, listOptions v1.ListOptions) error {
	var timeout time.Duration
	if listOptions.TimeoutSeconds != nil {
		timeout = time.Duration(*listOptions.TimeoutSeconds) * time.Second
	}
	return c.client.Delete().
		Namespace(c.ns).
		Resource("supernicos").
		VersionedParams(&listOptions, scheme.ParameterCodec).
		Timeout(timeout).
		Body(options).
		Do().
		Error()
}

// Patch applies the patch and returns the patched superNico.
func (c *superNicos) Patch(name string, pt types.PatchType, data []byte, subresources ...string) (result *v1alpha1.SuperNico, err error) {
	result = &v1alpha1.SuperNico{}
	err = c.client.Patch(pt).
		Namespace(c.ns).
		Resource("supernicos").
		SubResource(subresources...).
		Name(name).
		Body(data).
		Do().
		Into(result)
	return
}
