package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// SuperNico is a specification for a SuperNico resource
type SuperNico struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec SuperNicoSpec `json:"spec"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type SuperNicoList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []SuperNico `json:"items"`
}

// SuperNicoSpec is the spec for a SuperNico resource
type SuperNicoSpec struct {
	DeploymentName string             `json:"deploymentName"`
	Replicas       *int32             `json:"replicas"`
	Container      SuperNicoContainer `json:"container"`
	Service        SuperNicoService   `json:"service"`
}

// SuperNicoContainer is the container definition for a SuperNico resource
type SuperNicoContainer struct {
	Name             string `json:"name"`
	Image            string `json:"image"`
	ContainerPort    int32 `json:"port"`
	ImagePullSecrets string `json:"imagePullSecrets"`
}

// SuperNicoService is the service definition for a SuperNico resource
type SuperNicoService struct {
	Type        string `json:"type"`
	ServicePort int32 `json:"port"`
}
