#Enable GO Mod
GO111MODULE=on

VERSION := 0.1.0-SNAPSHOT

# #######################
# Generate CR Api Client.
# #######################

gen:
	./hack/update-codegen.sh

# #######################
# BUILD THE GO Controller (gc)
# #######################
gc:
	rm -rf $(GOPATH)/bin/controller
	go build -v -i -o $(GOPATH)/bin/controller ./cmd/controller

# local build, exe will be in bin directory
build-gc:
	rm -rf bin/controller
	go build -v -i -o bin/controller ./cmd/controller

# build linux cross compilation
build-linux-gc:
	rm -rf bin/linux/controller
	GOOS=linux GOARCH=amd64 go build -v -i -o bin/linux/controller/app ./cmd/controller

build-image-gc:
	docker build -t registry.gitlab.com/ngilmant/training-go-3/controller -f Dockerfile .



