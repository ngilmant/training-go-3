FROM alpine:3.7

# create data volume
RUN mkdir -p /home/ngi/data
VOLUME /home/ngi/data

# add app
COPY /bin/linux/controller/app /home/ngi/app

RUN [ '-f home/ngi/app' ] && echo "Executable file exists" || echo "Executable file does not exist"
ENTRYPOINT /home/ngi/app