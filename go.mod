module gitlab.com/ngilmant/training-go-3

go 1.12

require (
	k8s.io/api v0.0.0-20190717022910-653c86b0609b
	k8s.io/apimachinery v0.0.0-20190717022731-0bb8574e0887
	k8s.io/client-go v0.0.0-20190717023132-0c47f9da0001
	k8s.io/code-generator v0.0.0-20190717022600-77f3a1fe56bb // indirect
	k8s.io/klog v0.3.1
)
